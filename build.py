from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(name= 'Generic model class',
      cmdclass = {'build_ext': build_ext},
      ext_modules = [Extension("src_build",["klp_onpm_tools/src_build.c"])])

from src_build import build_c_src
import os
import site 
import glob
os.remove(glob.glob(os.path.join("*.so"))[0])
path = os.path.join(site.getsitepackages()[0], "klp_onpm_tools")
build_c_src(path)
