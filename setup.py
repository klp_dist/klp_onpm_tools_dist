import setuptools
import site
import os
from setuptools.command.install import install

class CythonizeInstall(install):
    def __init__(self, dist):
        super(install, self).__init__(dist)
        
    def run(self):
        install.run(self)
        self.__post_install()
        
    def __post_install(self):
        from klp_onpm_tools.src_build import build_src
        path = os.path.join(site.getsitepackages()[0], "klp_onpm_tools")
        build_src(path)

setuptools.setup(
    name="klp_onpm_tools",
    version="1.0",
    author="Kalapa",
    author_email="thangbm@kalapa.vn",
    description="Kalapa Onpremise Tools",
    long_description="",
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        "pycryptodome", "cryptography", "Cython"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # cmdclass={'install': CythonizeInstall}
)
